import React from 'react';

const SearchForm = ({ searchboxItems, onSearch, onReset }) => {
  const [searchCriteria, setSearchCriteria] = React.useState(
    Object.fromEntries(searchboxItems.map(item => [item?.name, '']))
  );

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setSearchCriteria(prevCriteria => ({
      ...prevCriteria,
      [name]: value
    }));
  };

  const handleSearch = () => {
    onSearch && onSearch(searchCriteria)
  };

  const handleReset = () => {
    setSearchCriteria(Object.fromEntries(searchboxItems.map(item => [item?.name, ''])));
    onReset && onReset()
  };
  return (
    <div className="search-form">
      {searchboxItems.map((item) => (
        <div key={item?.name}>
          <label>{item?.label}</label>
          {item?.type === 'input' && (
            <input
              type="text"
              name={item?.name}
              value={searchCriteria[item?.name]}
              onChange={handleInputChange}
            />
          )}
          {item?.type === 'select' && Array.isArray(item?.options) && (
            <select
              name={item?.name}
              value={searchCriteria[item?.name]}
              onChange={handleInputChange}
            >
              {item.options.map((option, optionIndex) => (
                <option key={optionIndex} value={option}>
                  {option}
                </option>
              ))}
            </select>
          )}
        </div>
      ))}
      <div className="buttons-row">
        <div className="recovery-btn">
          <button onClick={handleReset}>بازنشانی</button>
        </div>
        <div className="search-btn">
          <button onClick={handleSearch}>جستجو</button>
        </div>
      </div>
    </div>
  );
};

export default SearchForm;