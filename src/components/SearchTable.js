import React, { useState } from 'react';
import SearchForm from './Search';
import TableComponent from './Table';

const SearchTable = ({ searchboxItems, columns, data }) => {
  const [filteredData, setFilteredData] = useState(data);

  const handleSearch = (searchCriteria) => {
    const newFilteredData = data.filter(item => {
      return searchboxItems.every(searchItem => {
        if (searchItem.type === 'input' || searchItem.type === 'select') {
          let itemValue;
          if (searchItem.name === 'relationEmployeeName') {
            itemValue = item?.variables?.relation_employee_name || '';
          } else {
            itemValue = item[searchItem?.name];
          }
          const searchCriteriaValue = searchCriteria[searchItem?.name];
          
          if (searchCriteriaValue) {
            return itemValue && itemValue.toLowerCase().includes(searchCriteriaValue.toLowerCase());
          } else {
            return true;
          }
        } else {
          return true;
        }
      });
    });
    setFilteredData && setFilteredData(newFilteredData);
  };

  const handleReset = () => {
    setFilteredData && setFilteredData(data);
  };

  return (
    <div className="search-component">
      <SearchForm searchboxItems={searchboxItems} onSearch={handleSearch} onReset={handleReset} />
      <TableComponent data={filteredData} columns={columns} />
    </div>
  );
};

export default SearchTable;